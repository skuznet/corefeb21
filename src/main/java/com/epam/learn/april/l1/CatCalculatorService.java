package com.epam.learn.april.l1;

import java.util.Date;

public class CatCalculatorService {

    // return cat count for feed
    int getCatCount(int[] catArray) {
        if (catArray.length > 100) {
            throw new IllegalArgumentException("Array should be less then 100: " + catArray.length);
        }
        if (catArray.length % 2 == 0) {
            return catArray.length;
        } else {
            if (catArray.length > 2) {
                return catArray.length - 1;
            } else {
                return 0;
            }
        }
    }
}
