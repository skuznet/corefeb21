package com.epam.learn.april.l1;

public class Main2<T> {
    private T t;

    public static void main(String[] args) {
        Main2<Long> main = new Main2();
        main.getCast(2L);
    }

    void getCast(T l) {
        System.out.println(l);
    }

}
