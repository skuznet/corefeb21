package com.epam.learn.april.l1.some1;

import com.epam.learn.april.l1.some2.Cat;

public class Animal {
    private String name;

    public Animal(String name) {
        this.name = name;
    }

    private void getName() {
        System.out.println("My name is: " + name);
    }

    public static void main(String[] args) {
        Animal cat = new Cat("Barsik");
        Animal animal = new Animal("Animal");
        cat.getName();
    }
}
