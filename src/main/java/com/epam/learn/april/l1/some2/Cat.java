package com.epam.learn.april.l1.some2;

import com.epam.learn.april.l1.some1.Animal;

public class Cat extends Animal {
    private String name;

    public Cat(String name) {
        super("Animal");
        this.name = name;
    }

    protected void getName() {
        System.out.println("I am cat!");
    }
}
