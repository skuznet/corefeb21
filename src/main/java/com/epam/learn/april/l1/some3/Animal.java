package com.epam.learn.april.l1.some3;

public interface Animal {
    void getName();
    void eat();
    void sleep();
}
