package com.epam.learn.april.l1.some3;

import java.util.concurrent.ExecutorService;

public class Cat implements Animal {
    public static void main(String[] args) {
        Animal cat = new Cat();
        cat.getName();
        cat.eat();
    }

    private void getDescription() {
    }

    @Override
    public void getName() {
        System.out.println("Barsik");
    }

    @Override
    public void eat() {
        System.out.println("I am eating");
    }

    @Override
    public void sleep() {
        System.out.println("I am sleep");
    }
}
