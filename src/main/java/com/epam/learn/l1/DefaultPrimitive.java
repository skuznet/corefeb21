package com.epam.learn.l1;

public class DefaultPrimitive {
    public static void main(String[] args) {
        System.out.println(getSum((byte)1, (byte)1));
    }

    // byte + byte, byte + short, short + int, int + int = int
    private static byte getSum (byte v1, byte v2) {
        v1 = 127;
        v2 = 1;
        return (byte) (v1 + v2);
    }

    private static long getIntSum (int v1, long v2) {
        return (long) v1 + v2;
    }

    private double getSomething (float v1, double v2) {
        double someValue = v1;
        float f1 = (float) v2;
        return v1 + v2;
    }
}
