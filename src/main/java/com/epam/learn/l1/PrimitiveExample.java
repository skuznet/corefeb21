package com.epam.learn.l1;

public class PrimitiveExample {
    // целые
    // контрол + шифт + стрелочка вврех или вниз
    // -128 127
    // 8 бит
    byte someByte;
    // -2^15 2^15 - 1
    // 16 бит
    short someShort;
    // -2^31 2^31 - 1
    // 32 бит
    int number = 1;
    // 64 бит
    // -2^63 2^63 - 1
    long bigNumber;

    // дробные
    // https://ru.wikipedia.org/wiki/IEEE_754-2008
    // 32 бит
    float nFloat;
    // 64 бит
    double nDouble;

    // символьный
    //16 бит
    char someChar;

    // логический
    // https://ru.stackoverflow.com/questions/625400/%D0%A0%D0%B0%D0%B7%D0%BC%D0%B5%D1%80-%D1%82%D0%B8%D0%BF%D0%B0-boolean
    boolean isTrue;
}
