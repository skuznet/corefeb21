package com.epam.learn.l10;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Stack;
import java.util.Vector;

public class Main2 {
    public static void main(String[] args) {
        Vector vector = new Vector(4, 2);
        System.out.println("Initial size: " + vector.size());
        vector.addElement(new Integer(2));
        vector.addElement(new Integer(3));
        vector.addElement(new Integer(4));
        vector.addElement(new Integer(5));
        System.out.println("total capacity: " + vector.capacity());
        vector.addElement(new Integer(1));
        System.out.println("total capacity: " + vector.capacity());
        System.out.println("Initial size: " + vector.size());
        vector.addElement(new Double(1.2));
        System.out.println(vector);

        Enumeration enumeration = vector.elements();
        System.out.println("elements in vector: ");
        while (enumeration.hasMoreElements()) {
            System.out.println(enumeration.nextElement() + " ");
        }

        Hashtable<String, String> map = new Hashtable<>();
        for (Enumeration<String> e = map.keys(); e.hasMoreElements();){
            String s = e.nextElement();
            System.out.println(s);
        }

    }
}
