package com.epam.learn.l10;

import java.util.*;

public class Main4 {
    public static void main(String[] args) {
        Hashtable<String, String> hashtable = new Hashtable<>();
        hashtable.put("1", "One");
        hashtable.put("2", "Two");
        hashtable.put("3", "Three");

        Collection c = hashtable.values();
        Iterator it = c.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }
        c.remove("One");

        Enumeration e = hashtable.elements();
        while (e.hasMoreElements()) {
            System.out.println(e.nextElement());
        }

        Properties capitals = new Properties();
        Set states;

        String str;
        capitals.put("Illinois", "Springfield");
        capitals.put("Washington", "Olympia");
        capitals.put("California", "Sacramento");

        states = capitals.keySet();
        Iterator it2 = states.iterator();
        while (it2.hasNext()) {
            str = (String) it2.next();
            System.out.println("The capital of " + str + "is " + capitals.getProperty(str) + ".");
        }
        System.out.println();
        str = capitals.getProperty("Florida", "Not Found");
        System.out.println("The capital of Florida is " + str + ".");
    }
}
