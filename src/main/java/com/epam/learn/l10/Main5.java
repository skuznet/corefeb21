package com.epam.learn.l10;

import java.util.ArrayList;
import java.util.BitSet;

public class Main5 {
    public static void main(String[] args) {
        BitSet bitSet = new BitSet();
        bitSet.set(0);
        bitSet.set(1);
        System.out.println(bitSet);
        bitSet.clear(0);
        System.out.println(bitSet);
        System.out.println(bitSet.length());
        System.out.println(bitSet.size());

        bitSet.set(2);
        bitSet.set(4);
        System.out.println(bitSet);
        System.out.println(bitSet.length());
        System.out.println(bitSet.size());
    }
}
