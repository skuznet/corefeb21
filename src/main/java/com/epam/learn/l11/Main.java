package com.epam.learn.l11;

import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Cat barsik = new Cat("Barsik", 2);
        Cat murzik = new Cat("Murzik", 1);

        System.out.println(barsik.hashCode());
        System.out.println(murzik.hashCode());
        System.out.println(barsik.equals(murzik));

        Set<Cat> set = new HashSet<>();
        set.add(barsik);
        set.add(murzik);

        System.out.println(set);


    }
}
