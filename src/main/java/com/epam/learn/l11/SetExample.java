package com.epam.learn.l11;

import java.util.HashSet;
import java.util.Set;

public class SetExample {

    public static void main(String[] args) {
        Set set = new HashSet();
        set.add("Cat");
        set.add("Cat");

        System.out.println("Cat".hashCode());
        System.out.println("Cat".hashCode());

        System.out.println(set);
    }

}
