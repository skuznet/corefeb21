package com.epam.learn.l12;

import java.util.Objects;

public class Barsik extends Cat {
    private int age;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Barsik barsik = (Barsik) o;
        return age == barsik.age;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), age);
    }

    public static void main(String[] args) {
        Barsik barsik = new Barsik();
        barsik.setName("Barsik");

        Barsik barsik2 = new Barsik();
        barsik2.setName("Barsik");

        Barsik barsik3 = new Barsik();
        barsik3.setName("Murzik");

        System.out.println(barsik.equals(barsik2));
    }
}
