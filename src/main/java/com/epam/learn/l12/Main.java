package com.epam.learn.l12;

import com.epam.learn.l11.Cat;

import java.util.*;

public class Main<E, K, V> {
    public static void main(String[] args) {
        Cat cat = new Cat("Barsik", 2);
        Collection collection;

    }

    public void getInfo() {
        // вне зависимости от реализации
//        List<E> list = new ArrayList<>();
        List<E> list = new LinkedList<>();
        list.add((E) new Cat("Barsik", 2));
        Set<E> set;
        Queue<E> queue;

        Map<K, V> map;
        Set<Map.Entry<K, V>> set2;

        AbstractList abstractList;
        HashSet hashSet;
        SortedSet sortedSet;
        TreeSet set1;
    }

    public void getInfo2() {
        Map<K, V> map;
        AbstractMap abstractMap;
        // посмотреть
        WeakHashMap weakHashMap;
        Hashtable hashtable;
        HashMap hashMap;
        SortedMap sortedMap;
        TreeMap treeMap;

        Queue queue;
        Deque deque;
    }
}
