package com.epam.learn.l12;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Main3 {
    public static void main(String[] args) {
        List<String> list = new LinkedList<>();
        list.add("barsik");
        list.add("murzik");

        // НЕЛЬЗЯ ИСПОЛЬЗОВАТЬ ДЛЯ УДАЛЕНИЯ ТЕКУЩЕГО ЭЛЕМЕНТА!!!
//        for (String value : list) {
//            System.out.println(value);
//        }

//        list.forEach(System.out::println);

        Iterator<String> iterator = list.iterator();
        // hasNext(); next(); remove();
//        iterator.hasNext();
//        iterator.next();
//        iterator.remove();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

    }
}
