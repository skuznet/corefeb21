package com.epam.learn.l12;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executor;

public class Main4 {
    public static void main(String[] args) {
        List<String> list = new LinkedList<>();
        list.add("barsik");
        list.add("barsik");
        list.add("barsik");
        list.add("murzik");
        list.add("murzik");
        list.add("murzik");
        list.add("murzik");
        list.add("barsik");
        list.add("barsik");
        list.add("barsik");

        // ИСПОЛЬЗОВАТЬ ДЛЯ УДАЛЕНИЯ ТЕКУЩЕГО ЭЛЕМЕНТА - НЕЛЬЗЯ!
        // java.util.ConcurrentModificationException
//        for (String name : list) {
//            if(name.equals("murzik")) {
//                list.remove(name);
//            }
//        }

//        Iterator<String> iterator = list.iterator();
//
//        while (iterator.hasNext()) {
//           String el = iterator.next();
//           if(el.equals("barsik")) {
//               iterator.remove();
//           }
//        }

// при работе с for с индексами большая вероятность человеческого фактора
        for (int i = 0; i < list.size(); i++) {
            if(list.get(i).equals("barsik")) {
                list.remove("barsik");
            }
        }

        // jdk 1.8, jre sdk == jdk

        // JAVA API + CLIENT

        // JAVA API - 1) из коробки 2) JSR - дополнительно ставить 3) API - все остальное не связанное с JSR

//        list.removeIf(O -> O.equals("barsik"));

        System.out.println(list);
    }
}
