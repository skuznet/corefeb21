package com.epam.learn.l13;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(3);
        list.add(2);
        list.add(4);

        // merge sort O(n * logn)
        Collections.sort(list);
        System.out.println(list);
        // log(n)
        System.out.println(Collections.binarySearch(list, 3));

        Collections.reverse(list);
        System.out.println(list);
        Collections.shuffle(list);
        System.out.println(list);

        List<Integer> list2 = new ArrayList<>(30);
        list2.add(1);
        list2.add(1);
        list2.add(1);
        list2.add(1);
        System.out.println(list2);
        Collections.fill(list2, 3);
        System.out.println(list2);

        List<String> list3 = Arrays.asList("Barsik", "Murzik", "Snegok");
        // o(logn)
        System.out.println("Index of Murzik is: " + Collections.binarySearch(list3, "Murzik"));
        // o(n)
        System.out.println(list3.indexOf("Murzik"));
//        Collections.fill(list3, "barsik");
        System.out.println(list3);

        Collection<String> collection = Arrays.asList("red", "blue", "green");
        System.out.println(Collections.max(collection));
        System.out.println(Collections.min(collection));

        Collections.copy(list, list2);

        System.out.println(list);

        list.addAll(list2);
        System.out.println(list);

        Collections.rotate(list3, 2);
        System.out.println(list3);

        boolean replaceIsSuccessful = Collections.replaceAll(list3,"asda", "Sharik");
        System.out.println(replaceIsSuccessful);
        System.out.println(list3);

    }
}
