package com.epam.learn.l13;

import java.util.*;

public class Main2 {
    public static void main(String[] args) {
        List<String> list = Arrays.asList("Barsik", "Pushok", "Barsik", "Murzik", "Snegok", "Rigik", "Barsik",
                "Murzik", "Snegok");
        List<String> list2 = Arrays.asList("Barsik", "Murzik", "Snegok");

        System.out.println(Collections.indexOfSubList(list, list2));
        System.out.println(Collections.lastIndexOfSubList(list, list2));

        Collections.swap(list2, 0, 2);
        System.out.println(list2);

//        Collection<String> collection = Collections.unmodifiableCollection(list2);

        // ctrl + /
//        collection.add("asd");
//        collection.remove("Barsik");
//        System.out.println(collection);

        Collection<String> collection = Collections.synchronizedCollection(list2);

        Collection collection1 = Collections.checkedCollection(list, String.class);
//        collection1.add(1);
        System.out.println(collection1);

        Set<String> singletonSet = Collections.singleton("Barsik");
        System.out.println(singletonSet);
//        singletonSet.add("asd");

        Collection<String> collection2 = Collections.nCopies(4, "Barsik");
        System.out.println(collection2);

        System.out.println(Collections.frequency(collection1, "Barsik"));

        Integer[] array = new Integer[]{2, 4, 1, 3};
        Arrays.sort(array, Collections.reverseOrder());
        System.out.println(Arrays.asList(array));

        Vector<String> vector = new Vector<>();
        vector.add("Barsik");
        vector.add("Murzik");
//        Enumeration<String> enumeration = vector.elements();
//        List<String> list1 = Collections.list(enumeration);
//        System.out.println(list1);
        List<String> list1 = new ArrayList<>();
        list1.addAll(vector);
        System.out.println(list1);

        List<String> list4 = new ArrayList<>();
        list4.add("Sharik");
        list4.add("Bobik");

        List<String> list5 = new ArrayList<>();
        list5.add("Sharik");
        list5.add("Reks");
//        Arrays

        boolean isUnique = Collections.disjoint(list1, list4);
        boolean isUnique2 = Collections.disjoint(list4, list5);
        System.out.println(isUnique);
        System.out.println(isUnique2);

        List<Integer> numList = new ArrayList<>();
        Collections.addAll(numList, array);
        System.out.println(numList);

        Map<String, Boolean> map = new HashMap<>();
//        map.put("Barsik", true);
        System.out.println(Collections.newSetFromMap(map));

        Deque<String> deque = new LinkedList<>();
        Queue<String> queue = Collections.asLifoQueue(deque);

    }
}
