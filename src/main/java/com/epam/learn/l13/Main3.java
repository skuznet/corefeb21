package com.epam.learn.l13;

import java.util.Arrays;
import java.util.Collections;

public class Main3 {
    public static void main(String[] args) {
        Dog[] array = new Dog[]{new Dog("Sharik"), new Dog("Makar"), new Dog("Bobik")};
        Arrays.sort(array, Collections.reverseOrder());
        System.out.println(array);
    }
}
