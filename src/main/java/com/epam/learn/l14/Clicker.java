package com.epam.learn.l14;

import java.util.concurrent.TimeUnit;

public class Clicker extends Thread {
    int click = 0;
    private volatile boolean running = true;

    public static void main(String[] args) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    TimeUnit.MINUTES.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.setDaemon(true);
        thread.start();

        for (int i = 0; i < 10 ; i++) {
            Thread thread2 = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        TimeUnit.MINUTES.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
            thread2.start();
        }
        try {
            TimeUnit.MINUTES.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
