package com.epam.learn.l14;

public class Demo3 {
    public static void main(String[] args) {
        Flyable bird = new Flyable() {
            @Override
            public void fly() {
                System.out.println("I am flying...");
            }
        };

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();

        System.out.println("Current thread is : " + thread.isAlive());
        System.out.println("Main thread is : " + Thread.currentThread().isAlive());
        System.out.println("Current thread class name is : " + thread.getClass().getName());
        System.out.println("Current thread is : " + thread.getState());
    }
}
