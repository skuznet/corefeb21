package com.epam.learn.l14;

public class Main {
    public static void main(String[] args) {
        // выполнение
        // готовность к выполнению
        // приостановлен
        // возобновлен
        // блокирован
        // завершен

        // Non-Existing -> New - > Runnable | Blocked - Waiting - Terminated -> Non-Existing

        Talk talk = new Talk();
        Thread walk = new Thread(new Walk());
//        Walk w = new Walk();
//        w.run();
//        talk.start();
//        walk.start();
        System.out.println(Thread.currentThread().getName());
    }
}
