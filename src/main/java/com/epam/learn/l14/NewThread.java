package com.epam.learn.l14;

public class NewThread implements Runnable {
    String name;
    Thread t;

    public NewThread(String threadName) {
        this.name = threadName;
        t = new Thread(this, name);
        System.out.println("New thread: " + t);
        t.start();
    }

    @Override
    public void run() {
        try {
            for (int i = 5; i > 0 ; i--) {
                System.out.println(name + ": " + i);
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(name + " exiting.");
    }
}
