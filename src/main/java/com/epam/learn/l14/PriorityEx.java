package com.epam.learn.l14;

public class PriorityEx {
    // Thread1 ---  ---
    // Thread2    --
    public static void main(String[] args) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println('1');
            }
        });
        System.out.println(Thread.MAX_PRIORITY);
        System.out.println(Thread.MIN_PRIORITY);
        thread.setPriority(Thread.NORM_PRIORITY);
    }
}
