package com.epam.learn.l15;

public class Account {
    private int balance;
    private final Object lock = new Object();

    public Account(int balance) {
        this.balance = balance;
    }

//    public synchronized void deposit (int amount) throws InterruptedException {
//        int x = balance + amount;
//        Thread.sleep(1L);
//        balance = x;
//    }

    public void deposit (int amount) throws InterruptedException {
        System.out.println("I am deposit : " + amount);
        synchronized (this) {
            int x = balance + amount;
            Thread.sleep(1L);
            balance = x;
        }
    }

//    public synchronized void withdraw (int amount) throws InterruptedException {
//        int x = balance - amount;
//        Thread.sleep(1L);
//        balance = x;
//    }

    public void withdraw (int amount) throws InterruptedException {
        System.out.println("I am withdraw : " + amount);
        synchronized (this) {
            int x = balance - amount;
            Thread.sleep(1L);
            balance = x;
        }
    }

    public int getBalance() {
        return balance;
    }
}
