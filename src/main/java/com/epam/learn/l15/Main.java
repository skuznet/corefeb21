package com.epam.learn.l15;

import java.util.concurrent.Executor;

public class Main {
    private final Object lock = new Object();

    // mutually exclusive lock
    // mutex
    // принцип туалета
    //    synchronized
    // wait
    // notify
    //
    // Deadlock
    // Livelock

    public static void main(String[] args) {

    }

    void doSomething() {
        System.out.println("I am here...");

        synchronized (lock) {

        }
    }
}
