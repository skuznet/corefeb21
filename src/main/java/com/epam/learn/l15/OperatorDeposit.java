package com.epam.learn.l15;

public class OperatorDeposit extends Thread {
    private Account account;

    public OperatorDeposit(Account account) {
        this.account = account;
    }

    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            try {
                account.deposit(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
