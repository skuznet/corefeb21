package com.epam.learn.l15;

public class OperatorWithdraw extends Thread {
    private Account account;

    public OperatorWithdraw(Account account) {
        this.account = account;
    }

    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            try {
                account.withdraw(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
