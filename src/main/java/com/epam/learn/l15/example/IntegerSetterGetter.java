package com.epam.learn.l15.example;

import java.util.Random;

public class IntegerSetterGetter extends Thread {
    private SharedResource resource;
    private boolean run;

    private Random random = new Random();

    public IntegerSetterGetter(String name, SharedResource resource) {
        super(name);
        this.resource = resource;
        run = true;
    }

    @Override
    public void run() {
        int action;

        try {
            while (run) {
                action = random.nextInt(1000);
                if (action % 2 == 0) {
                    getIntegersFromResource();
                } else {
                    setIntegersIntoResource();
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void setIntegersIntoResource() {
        Integer number = random.nextInt(500);
        synchronized (resource) {
            resource.setElement(number);
            System.out.println("Thread " + getName() + " write a number " + number);
            resource.notify();
        }
    }

    private void getIntegersFromResource() throws InterruptedException {
        Integer number;

        synchronized (resource) {
            System.out.println("Thread " + getName() + " want to retrieve number");
            number = resource.getElement();
            while (number == null) {
                System.out.println("Thread " + getName() + " waiting while queue is going to be full");
                resource.wait();
                System.out.println("Thread " + getName() + " restore work");
                number = resource.getElement();
            }
            System.out.println("Thread " + getName() + " retrieved number " + number);
        }
    }

    public void stopThread() {
        run = false;
    }
}
