package com.epam.learn.l15.example;

public class Main {
    public static void main(String[] args) {
        SharedResource sharedResource = new SharedResource();
        for (int i = 0; i < 5; i++) {
            IntegerSetterGetter integerSetterGetter = new IntegerSetterGetter("Thread" + i, sharedResource);
            integerSetterGetter.start();
        }
    }
}
