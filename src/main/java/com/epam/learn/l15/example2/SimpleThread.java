package com.epam.learn.l15.example2;

public class SimpleThread implements Runnable {
    public int count = 0;

    @Override
    public void run() {
        for (int i = 0; i < 1_000_000; i++) {
            count++;
        }
        System.out.println(count);
    }
}
