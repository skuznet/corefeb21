package com.epam.learn.l15.example2;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Solution {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.execute(new SimpleThread());
        executorService.execute(new SimpleThread());
        executorService.shutdown();
    }
}
