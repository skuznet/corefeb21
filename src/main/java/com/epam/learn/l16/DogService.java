package com.epam.learn.l16;

public class DogService {
    public int getDogCount(boolean isTrue) {
        if (isTrue) {
            return 2;
        } else {
            return 1;
        }
    }
}
