package com.epam.learn.l16;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Main {
    // steam
    // in / out

    // source
    // destination

    // text-streams (символьные потоки3 - последовательность 16-битовых символов Unicode)
    // байтовые потоки (binary streams)

    // open for read/write close!
    // create object
    public static void main(String[] args) {
        // before java 7
//        PrintWriter pw = null;
//        try {
//            // create stream obj | open stream
//            FileWriter out = new FileWriter("text.txt");
//
//            // set settings
//            BufferedWriter br = new BufferedWriter(out);
//            pw = new PrintWriter(br);
//            pw.println("I am a sentence in a text-file.");
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            if (pw != null) {
//                 pw.close();
//            }
//        }

        // since java 7 (try with resources)
         try (PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter("text2.txt")))) {
             // work with stream from stream obj
             pw.println("Hello, I am from java 7 - 1");
         } catch (IOException e) {
             e.printStackTrace();
         }

    }
}
