package com.epam.learn.l16;

import java.io.*;

public class Main2 {
    public static void main(String[] args) {

//        CharConversionException
//        FileNotFoundException
//        InvalidClassException
//        IOException
//        NotSerializableException
//        OptionalDataException
//        SyncFailedException
//        UTFDataFormatException
//        EOFException
//        InterruptedException
//        InvalidObjectException
//        NotActiveException
//        ObjectStreamException
//        UnsupportedOperationException
//        WriteAbortedException

        OutputStream stdout = System.out;

        try {
            stdout.write(104);
            stdout.write(105);
            stdout.write(10);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
