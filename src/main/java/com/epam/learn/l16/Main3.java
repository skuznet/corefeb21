package com.epam.learn.l16;

import java.io.*;
import java.util.zip.ZipInputStream;

public class Main3 {
    public static void main(String[] args) {
        // types -> text/binary
        // ways -> input/output
        // type of I/O - Memory | Pipe | File | Object Serialization

        // text
        Reader reader;
        Writer writer;

        // byte-input
        InputStream inputStream;
        // byte output
        OutputStream outputStream;
    }

    void getReaderReal() {
        PipedReader pipedReader;
        CharArrayReader charArrayReader;
        StringReader stringReader;
        BufferedReader bufferedReader;
        FilterReader filterReader;
        InputStreamReader inputStreamReader;
        FileReader fileReader;
    }

    void getWriterReal() {
        PipedWriter pipedWriter;
        CharArrayWriter charArrayWriter;
        StringWriter stringWriter;
        BufferedWriter bufferedWriter;
        PrintWriter printWriter;
        OutputStreamWriter outputStreamWriter;
        FileWriter writer;
    }

    void getInputStreamReal() {
        ByteArrayInputStream arrayInputStream;
        FileInputStream fileInputStream;
        SequenceInputStream sequenceInputStream;
        ObjectInputStream objectInputStream;
        PipedInputStream pipedInputStream;
        FilterInputStream filterInputStream;
        BufferedInputStream bufferedInputStream;
        DataInputStream dataInputStream;
        PushbackInputStream pushbackInputStream;
        ZipInputStream zipInputStream;
    }

    void getOutputStreamReal() {
        ByteArrayOutputStream byteArrayOutputStream;
        FileOutputStream fileOutputStream;
        PipedOutputStream pipedOutputStream;
        ObjectOutputStream objectOutputStream;
        FilterOutputStream filterOutputStream;
        PrintStream printStream;
    }
}
