package com.epam.learn.l16;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.FilterOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintStream;
import java.io.PushbackInputStream;
import java.io.SequenceInputStream;

public class Main4 {
    public static void main(String[] args) {
        // InputStream | OutputStream
        // абстрактный класс, описывающий поточный ввод
        InputStream inputStream;
        // абстрактный класс, описывающий поточный вывод
        OutputStream outputStream;

        // для буферезации ввода
        BufferedInputStream bufferedInputStream;
        // для буферезации вывода
        BufferedOutputStream bufferedOutputStream;
        // это поток читающий из массива байт
        ByteArrayInputStream byteArrayInputStream;
        // поток пишущий в массив байт
        ByteArrayOutputStream byteArrayOutputStream;
        // поток ввода, который содержит методы для чтения данных стандартных типов java
        DataInputStream dataInputStream;
        // содержит методы для записи стандартных типов Java
        DataOutputStream dataOutputStream;
        // читает байты из файла
        FileInputStream fileInputStream;
        // пишет байты в файл
        FileOutputStream fileOutputStream;
        // adapter pattern
        FilterInputStream filterInputStream;
        // adapter pattern
        FilterOutputStream filterOutputStream;

        // поток сериализации объектов
        ObjectInputStream objectInputStream;
        // для десериализации объектов
        ObjectOutputStream objectOutputStream;
        // Поток читающий данные из канала
        PipedInputStream pipedInputStream;
        // Поток пишущий данный в канал
        PipedOutputStream pipedOutputStream;

        // используется для конвертации и записи строк в байтовый поток
        PrintStream printStream;
        // позволяет вернуть в поток считанные из него данные
        PushbackInputStream pushbackInputStream;
        // считывает данные из других двух и более входных потоков
        SequenceInputStream sequenceInputStream;


    }
}
