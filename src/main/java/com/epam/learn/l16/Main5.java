package com.epam.learn.l16;

import java.io.*;

public class Main5 {
    public static void main(String[] args) throws IOException {
        byte[] bytesToWrite = {1, 2, 3};
        byte[] bytesRead = new byte[10];

        String fileName = "c:\\test\\test.txt";
        FileOutputStream outFile;
        FileInputStream inFile;

        outFile = new FileOutputStream(fileName);
        System.out.println("File is opened for write");
        outFile.write(bytesToWrite);
        System.out.println("Wrote " + bytesToWrite.length + " bytes");
        outFile.close();
        System.out.println("Out stream closed");

        inFile = new FileInputStream(fileName);
        System.out.println("File opened for read");
        int bytesAvailable = inFile.available();
        System.out.println("Ready to read " + bytesAvailable + " bytes");

        int count = inFile.read(bytesRead, 0, bytesAvailable);
        System.out.println("Read " + count + " bytes");
        inFile.close();
        System.out.println("In stream closed");
    }
}
