package com.epam.learn.l16;

import java.io.*;

public class Main6 {
    public static void main(String[] args) throws IOException {
        PipedOutputStream pipedOutputStream;
        PipedInputStream pipedInputStream;

        int countread = 0;
        int[] toRead = null;

        pipedInputStream = new PipedInputStream();
        pipedOutputStream = new PipedOutputStream(pipedInputStream);

        for (int i = 0; i < 20; i++) {
            pipedOutputStream.write(i);
        }

        int willRead = pipedInputStream.available();
        toRead = new int[willRead];

        for (int i = 0; i < willRead; i++) {
            toRead[i] = pipedInputStream.read();
            System.out.print(toRead[i] + " ");
        }

        FileInputStream inFile2;
        FileInputStream inFile3;

        SequenceInputStream sequenceInputStream;
        FileOutputStream outFile;

        // i am file 2.
        inFile2 = new FileInputStream("file 2.txt");
        // i am file 3.
        inFile3 = new FileInputStream("file 3.txt");
        sequenceInputStream = new SequenceInputStream(inFile2, inFile3);
        // i am file2.i am file3.
        outFile = new FileOutputStream("file 4.txt");
        int readByte = sequenceInputStream.read();
        while (readByte != -1) {
            outFile.write(readByte);
            readByte = sequenceInputStream.read();
        }
    }
}
