package com.epam.learn.l17;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Ex {
    public static void main(String[] args) throws IOException {
        OutputStream  stdout = System.out;
        stdout.write(104);
        stdout.flush();
        stdout.write('\n');

        byte[] bytes = new byte[5];
        InputStream stdin1 = System.in;
        stdin1.read(bytes);
        System.out.write(bytes);
        System.out.write('\n');
        System.out.flush();
    }
}
