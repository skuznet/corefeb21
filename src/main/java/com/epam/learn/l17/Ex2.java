package com.epam.learn.l17;

import java.io.*;
import java.util.Scanner;

public class Ex2 {
    public static void main(String[] args) throws IOException {
//        BufferedReader in = new BufferedReader(new FileReader("foo.in"));
//        BufferedWriter out = new BufferedWriter(new FileWriter("foo.in"));

        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextInt()) {
            int n = scanner.nextInt();
            System.out.println(n);
        }
    }
}
