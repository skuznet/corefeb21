package com.epam.learn.l17;

import java.io.*;

public class Ex5 implements Serializable{
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        Object objSave = 1;
        ObjectOutputStream oos = new ObjectOutputStream(os);
        oos.writeObject(objSave);

        byte[] bytes = os.toByteArray();
        for (byte b : bytes) {
            System.out.print((char) b);
        }
        System.out.println();

        ByteArrayInputStream is = new ByteArrayInputStream(bytes);
        ObjectInputStream ois = new ObjectInputStream(is);
        Object objRead = ois.readObject();
        System.out.printf("read object is : %s%n", objRead);
        System.out.printf("Object equality is : %s%n", objSave.equals(objRead));
        System.out.printf("Reference equality is : %s%n", objSave == objRead);

    }
}
