package com.epam.learn.l17;

import java.io.*;

public class Main {
    // Reader Writer
    // read()
    // write()

    public static void main(String[] args) {
        Reader reader;
        Writer writer;

        BufferedReader bufferedReader;
        BufferedWriter bufferedWriter;
        CharArrayReader charArrayReader;
        CharArrayWriter charArrayWriter;

        FileReader fileReader;
        FileWriter fileWriter;

        FilterReader filterReader;
        FilterWriter filterWriter;

        InputStreamReader inputStreamReader;
        OutputStreamWriter outputStreamWriter;
        LineNumberReader lineNumberReader;

        PipedReader pipedReader;
        PipedWriter pipedWriter;

        PrintWriter printWriter;

        PushbackReader pushbackReader;

        StringReader stringReader;
        StringWriter stringWriter;
    }
}
