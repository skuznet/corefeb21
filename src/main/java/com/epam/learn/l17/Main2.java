package com.epam.learn.l17;

import java.io.CharArrayReader;
import java.io.IOException;

public class Main2 {
    public static void main(String[] args) throws IOException {
        String temp = "asdasdafdafqiegqigfqfi";
        int length = temp.length();
        char[] chars = new char[length];

        temp.getChars(0, length, chars, 0);

        CharArrayReader input1 = new CharArrayReader(chars);
        CharArrayReader input2 = new CharArrayReader(chars, 4, 5);

        int i;

        System.out.println("Input1: ");
        while ((i = input1.read()) != -1) {
            System.out.print((char) i);
        }

        System.out.println();
        System.out.println("Input2: ");
        while ((i = input2.read()) != -1) {
            System.out.print((char) i);
        }
    }

}
