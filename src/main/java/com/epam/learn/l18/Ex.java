package com.epam.learn.l18;

import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class Ex {
    public static void main(String[] args) {
        IntStream intStream; // int
        LongStream longStream; // long
        DoubleStream doubleStream; // double

//        Stream.empty().forEach(System.out::println);
//        Stream.of(1, 2, 3).forEach(System.out::println);
//        Arrays.asList(1, 2, 3).stream().forEach(System.out::println);

//        String value = Math.random() > 0.8 ? "I am happy" : null;
//        Stream.ofNullable(value).forEach(System.out::println);

//        Stream.generate(() -> 10).limit(2).forEach(System.out::println);
//        Stream.iterate(2, s -> s + 3).limit(5).forEach(System.out::println);
//        Stream.iterate(2, s -> s < 120, s -> s + 3).forEach(System.out::println);
//        Stream.concat(
//                Stream.of(1,2,3,4,5),
//                Stream.of(6,7,8,9,10)
//        ).forEach(System.out::println);

//        Stream.Builder<Integer> stream = Stream.<Integer>builder().add(1).add(2);
//        for (int i = 0; i < 10; i++) {
//            stream.accept(i);
//        }
//        stream.add(22).add(23).build().forEach(System.out::println);

//        IntStream.range(-10, 0).forEach(System.out::println);
//        Stream.of(1, 2, 3).filter(x -> x > 5).forEach(System.out::println);
//        Stream.of(11, 2, 32).filter(x -> x > 5).forEach(System.out::println);
        IntStream.range(2, 12).filter(o -> o % 2 == 0).forEach(System.out::println);
    }
}
