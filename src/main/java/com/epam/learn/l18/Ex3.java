package com.epam.learn.l18;

public class Ex3 {
    public static void main(String[] args) {
        FuncI funcI = new FuncI() {
            @Override
            public void getResult(int i) {
                System.out.println(" Hello from anonymous realisation " + i);
            }
        };

        funcI.getResult(2);

        FuncI funcI1 = i -> {
            System.out.println("Hello from lyambda " + i);
            System.out.println("Hello from lyambda " + i);
            System.out.println("Hello from lyambda " + i);
            System.out.println("Hello from lyambda " + i);
        };

        funcI1.getResult(2);

    }
}
