package com.epam.learn.l18;

import java.util.function.Predicate;

public class Ex4 {
    public static void main(String[] args) {
//        FuncL funcL = new FuncL() {
//            @Override
//            public String getName() {
//                return "barsik";
//            }
//        };
//
//        System.out.println(funcL.getName());
//
//        FuncL funcL1 = () -> "Barsik";
//
//        System.out.println(funcL1.getName());

        FuncL funcL2 = (owner, name) -> {
            System.out.println(owner + " taking a dog with name: " + name);
            return name;
        };

        System.out.println(funcL2.getName("Jack", "Sharik"));
    }
}
