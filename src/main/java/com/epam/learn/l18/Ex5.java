package com.epam.learn.l18;

import java.util.function.BiConsumer;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class Ex5 {
    public static void main(String[] args) {
        Predicate<String> predicate = (o) -> o.equals("barsik");
        boolean isTrue = predicate.test("barsik");
        System.out.println(isTrue);

        Supplier<String> supplier = () -> "barsik";
        System.out.println(supplier.get());

        BiConsumer<Integer, String> biConsumer = (i, s) -> {
            System.out.println("Count of " + s + " is: " + i);
        };

        biConsumer.accept(10, "dogs");
    }
}
