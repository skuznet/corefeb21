package com.epam.learn.l18;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class Main4 {
    public static void main(String[] args) {
        final List<Integer> list = new ArrayList<>();
        IntStream.range(0, 10000000).parallel().forEach(o -> list.add(o));
        System.out.println(list.size());
    }
}
