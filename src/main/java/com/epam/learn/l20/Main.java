package com.epam.learn.l20;

import java.util.concurrent.TimeUnit;

public class Main {
    public static void main(String[] args) {
        try {
            for (int i = 0; i < 5; i++) {
                Thread thread = new Thread(() -> {
                    try {
                        TimeUnit.SECONDS.sleep(30);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                });
                thread.start();
            }

            TimeUnit.SECONDS.sleep(30);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
