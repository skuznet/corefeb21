package com.epam.learn.l21;

public class BinarySearchEx {
    public static void main(String[] args) {
        int[] array = new int[]{1, 3, 5, 8, 11, 15, 18, 19, 21, 23, 154, 244};
        BinarySearchEx binarySearchEx = new BinarySearchEx();
        binarySearchEx.binarySearch(array, 0, array.length - 1, 23);
    }

    void binarySearch(int[] array, int first, int last, int item) {
        int position;
        int comparisonCount = 1;

        position = (first + last) / 2;

        while ((array[position]) != item && (first <= last)) {
            comparisonCount++;

            if (array[position] > item) {
                last = position - 1;
            } else {
                first = position + 1;
            }
            position = (first + last) / 2;
        }

        if (first <= last) {
            System.out.println(item + " is " + ++position + " in array");
            System.out.println("we found an item with : " + comparisonCount);
        } else {
            System.out.println("element not in array");
        }
    }
}
