package com.epam.learn.l21;

import java.util.Arrays;

public class BubbleSortEx {

    // O(n2)

    public static void main(String[] args) {
        int[] array = new int[]{1, 23, 4, 3, 12, 5};
        System.out.println(Arrays.toString(sort(array)));
    }

    private static int[] sort(int[] data) {
        int dataLength = data.length;
        int swap;
        boolean isSorted;

        for (int i = 0; i < dataLength; i++) {
            isSorted = true;
            for (int j = 1; j < (dataLength - i); j++) {
                if (data[j - 1] > data[j]) {
                    swap = data[j - 1];
                    data[j - 1] = data[j];
                    data[j] = swap;
                    isSorted = false;
                }
            }
            if (isSorted) {
                break;
            }
        }
        return data;
    }
}
