package com.epam.learn.l21;

public class Main {
    private static int[] array = new int[]{1, 3, 5, 8, 11, 15, 18, 19, 21, 23, 154, 244};

    public static void main(String[] args) {
        // O
        // O(1) - константная сложность
        // O(log n) -логарифмическая сложность
        // O(n) - линейная сложность
        // O(n*log(n)) - квазилинейная сложность
        // O(n2) - квадратичная сложность
        // O(n3) -
        int i = 154;
        boolean isInArray = false;
        int countOfTry = 0;

        for (int j = 0; j < array.length; j++) {
            countOfTry++;
            if (array[j] == i) {
                isInArray = true;
                break;
            }
        }

        System.out.println("The number: " + i + " in array: " + isInArray + " with count of try: " + countOfTry);
    }
}
