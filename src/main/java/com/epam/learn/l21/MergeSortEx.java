package com.epam.learn.l21;

import java.util.Arrays;

public class MergeSortEx {
    // n*log(n)
    public static void main(String[] args) {
        int[] array = new int[]{1, 23, 4, 3, 12, 5};
        System.out.println(Arrays.toString(sort(array)));
    }

    private static int[] sort(int[] array) {
        if (array == null) {
            return null;
        }

        if (array.length < 2) {
            return array;
        }

        int[] arrayB = new int[array.length / 2];
        System.arraycopy(array, 0, arrayB, 0, array.length / 2);

        int[] arrayC = new int[array.length - array.length / 2];
        System.arraycopy(array, array.length / 2, arrayC, 0, array.length - array.length / 2);

        arrayB = sort(arrayB);
        arrayC = sort(arrayC);

        return mergeArray(arrayB, arrayC);
    }

    private static int[] mergeArray(int[] arrayA, int[] arrayB) {
        int[] arrayC = new int[arrayA.length + arrayB.length];
        int positionA = 0;
        int positionB = 0;

        for (int i = 0; i < arrayC.length; i++) {
            if (positionA == arrayA.length ) {
                arrayC[i] = arrayB[positionB];
                positionB++;
            } else if (positionB == arrayB.length) {
                arrayC[i] = arrayA[positionA];
                positionA++;
            } else if (arrayA[positionA] < arrayB[positionB]) {
                arrayC[i] = arrayA[positionA];
                positionA++;
            } else {
                arrayC[i] = arrayB[positionB];
                positionB++;
            }
        }
        return arrayC;
    }
}
