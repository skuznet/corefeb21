package com.epam.learn.l4;

import java.util.Arrays;

public class ArrayEx1 {
    public static void main(String[] args) {
        int i = 5; int i2 = 11; int i3 = 7;
        Integer[] array = new Integer[5];
        array[0] = i;
        array[1] = i2;
        array[2] = i3;

        System.out.println(Arrays.toString(array));

//        int[] array2 = new int[]{5, 11, 2, 6};
//
//        System.out.println(Arrays.toString(array2));
//        System.out.println(array2.length);
        System.out.println(Runtime.getRuntime().totalMemory());
        System.out.println(Runtime.getRuntime().freeMemory());
    }
}
