package com.epam.learn.l4;

import java.util.Arrays;

public class ArrayEx2 {
    public static void main(String[] args) {
        Cat barsik = new Cat("barsik", 2);
        Cat muzrik = new Cat("murzik", 1);

        Cat[] catsArray = new Cat[2];
        catsArray[0] = barsik;
        catsArray[1] = muzrik;

        Cat[] cats = new Cat[]{barsik, muzrik};

        System.out.println(Arrays.toString(cats));
    }
}
