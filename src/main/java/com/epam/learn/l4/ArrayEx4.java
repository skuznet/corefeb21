package com.epam.learn.l4;

import java.util.Arrays;

public class ArrayEx4 {
    public static void main(String[] args) {
        int[][] array = new int[2][];

        array[0] = new int[2];
        array[0][1] = 2;

        array[1] = new int[5];

        System.out.println(array[0][2]);
    }
}
