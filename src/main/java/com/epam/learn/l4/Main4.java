package com.epam.learn.l4;

public class Main4 {
    public static void main(String[] args) {
        Cat barsik = new Cat("barsik", 2);
        Cat murzik = new Cat("murzik", 1);
        Cat pushok = new Cat("pushok", 3);
        Dog sharik = new Dog("sharik", 7);

//        Cat[] cats = new Cat[]{barsik, murzik, pushok};
        Animal[] animals = new Animal[]{barsik, murzik, pushok, sharik};

        for (Animal animal: animals) {
            Cat cat = (Cat) animal;
            System.out.println(animal.toString());
        }
    }
}
