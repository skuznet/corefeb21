package com.epam.learn.l7;

public class Cat implements Cloneable{
    //@Autowired
    private Barsik barsik;

    public Cat() {
    }

    public String getCatname() {
        return barsik.getName();
    }

    public void setBarsik(Barsik barsik) {
        this.barsik = barsik;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
