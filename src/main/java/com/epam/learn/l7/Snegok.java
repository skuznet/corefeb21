package com.epam.learn.l7;

import java.util.Objects;

public class Snegok {
    private String name;

    public Snegok(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Snegok{" +
                "name='" + name + '\'' +
                '}';
    }

    @Override
    public int hashCode() {
        return 1;
    }
}
