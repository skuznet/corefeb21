package com.epam.learn.l8;

public interface Flyable {
    void fly();

    interface SomeInnerInterface {
       void doSomething();
    }
}
