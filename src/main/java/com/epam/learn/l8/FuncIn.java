package com.epam.learn.l8;

@FunctionalInterface
public interface FuncIn {
    void getSomething();

    boolean equals(Object obj);
}
