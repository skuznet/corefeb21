package com.epam.learn.l8;

public interface Payable {
    void pay();
    void check();
}
