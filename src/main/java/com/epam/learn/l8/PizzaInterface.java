package com.epam.learn.l8;

public interface PizzaInterface extends Payable, Checker {
//    static void sayHi() {
//        System.out.println("Static hello from interface");
//    }
//
//    default void sayHello() {
//        System.out.println("hello from interface");
//    }

    void wash();

    void cook();

    void delivery();
}
