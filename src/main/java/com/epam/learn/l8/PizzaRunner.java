package com.epam.learn.l8;

import com.epam.learn.l8.impl.PizzaMSK;
import com.epam.learn.l8.impl.PizzaSpb;

public class PizzaRunner {
    private PizzaInterface pizzaMsk;
    private PizzaInterface pizza;
    private PizzaInterface pizzaSpb;

    public static void main(String[] args) {
        PizzaRunner runner = new PizzaRunner();
        runner.getPizzaInfo();
    }

    private void getPizzaInfo() {
        if (true) {
            pizza = new PizzaMSK();
        } else {
            pizza = new PizzaSpb();
        }

        pizzaMsk = new PizzaMSK();
        pizzaSpb = new PizzaMSK();
//        pizzaSpb.sayHello();
        pizzaMsk.wash();
        pizzaSpb.wash();
        pizzaMsk.cook();
        pizzaSpb.cook();
        pizzaMsk.delivery();
        pizzaSpb.delivery();
    }
}
