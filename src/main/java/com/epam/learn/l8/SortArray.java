package com.epam.learn.l8;

import java.util.Arrays;
import java.util.Comparator;

public class SortArray {
    public static void main(String[] args) {
        int[] array = {3, 5, 6, 1, 2, 9, 8};
        Arrays.sort(array);
        System.out.println(Arrays.toString(array));

    }
}
