package com.epam.learn.l8.compare;

public class CircleGO extends GeometricObject {
    private double radius;

    public CircleGO(double radius) {
        this.radius = radius;
    }

    @Override
    public double getArea() {
        return Math.PI * radius * radius;
    }
}
