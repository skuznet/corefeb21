package com.epam.learn.l8.compare;

import java.util.Comparator;

public class GeometricObjectComparator implements Comparator<GeometricObject> {
    @Override
    public int compare(GeometricObject o1, GeometricObject o2) {
        double area1 = o1.getArea();
        double area2 = o2.getArea();

//        return Double.compare(area1, area2);
        return (int) Math.signum(area1 - area2);
    }
}
