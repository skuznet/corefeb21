package com.epam.learn.l8.compare;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

public class Main {
    public static void main(String[] args) {
        Comparator<GeometricObject> comparator = new GeometricObjectComparator();
        Set<GeometricObject> set = new TreeSet<>(comparator);
        set.add(new RectangleGO(4, 5));
        set.add(new CircleGO(40));
        set.add(new CircleGO(40));
        set.add(new RectangleGO(4, 1));

        System.out.println(new CircleGO(40).hashCode());
        System.out.println(new CircleGO(40).hashCode());

        System.out.println("A sorted set of geometric objects");

        for (GeometricObject s : set) {
            System.out.println("area = " + s.getArea());
        }
    }
}
