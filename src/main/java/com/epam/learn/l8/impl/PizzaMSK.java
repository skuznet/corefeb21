package com.epam.learn.l8.impl;

import com.epam.learn.l8.PizzaInterface;

public class PizzaMSK implements PizzaInterface {
    @Override
    public void wash() {
        System.out.println("I am washing hands");
    }

    @Override
    public void cook() {
        System.out.println("cooking");
    }

    @Override
    public void delivery() {
        System.out.println("sending pizza to clients");
    }

    @Override
    public void pay() {
        System.out.println("i am paying...");
    }

    @Override
    public void check() {

    }
}
