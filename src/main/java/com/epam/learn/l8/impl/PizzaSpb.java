package com.epam.learn.l8.impl;

import com.epam.learn.l8.PizzaInterface;

public class PizzaSpb implements PizzaInterface {
    @Override
    public void wash() {
        System.out.println("I am washing hands");
    }

    @Override
    public void cook() {
        System.out.println("cooking with fish");
    }

    @Override
    public void delivery() {
        System.out.println("sending pizza to clients");
    }

    @Override
    public void pay() {
        System.out.println("free pizza");
    }

    @Override
    public void check() {

    }
}
