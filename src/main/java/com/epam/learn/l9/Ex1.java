package com.epam.learn.l9;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Ex1 {
    public static void main(String[] args) {
        System.out.println("Hello!");
//        throw new RuntimeException("I am exception");
//        System.out.println("By!");

        // throw - выброс исключения
        // throws - перекладываю ответственность обработки исключения на кого то еще
        // try - попытка
        // catch - отлавливаем исключительное событие
        // finally - блок кода который будет выполняться всегда
        try {
            throw new RuntimeException("I am exception");
        } catch (Error e) {
            e.printStackTrace();
        } finally {
            System.out.println("Finally");
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("By");
    }

    void getMoney() throws IOException {
        throw new IOException();
    }

    void getMoney2() throws RuntimeException {
        throw new RuntimeException();
    }

    void getInfo() {
        try {
            getMoney();
        } catch (IOException e) {
            e.printStackTrace();
        }

        getMoney2();
    }
}
