package com.epam.learn.l9;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Ex2 {

    public static void main(String[] args) {
        try {
            // open connection
            throw new RuntimeException();

        } catch (RuntimeException e) {
            //
        } finally {
//            System.out.println("connection close");
            // close connection
        }

//        try {
//            System.out.println("connection open");
//            throw new RuntimeException();
//        } finally {
//            System.out.println("connection close");
//        }

//        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//        try {
//            System.out.println(reader.readLine());
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                reader.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            System.out.println(reader.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
