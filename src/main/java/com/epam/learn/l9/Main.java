package com.epam.learn.l9;

import java.util.Optional;

public class Main<T1, T2> {
    T1 id;
    T2 name;

    public static void main(String[] args) {
        Main<Integer, String> ob = new Main<>();
        ob.id = 2;

        Main<String, String> ob2 = new Main<>();
        ob2.id = "asdasd";
    }
}
