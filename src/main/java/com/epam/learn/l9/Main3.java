package com.epam.learn.l9;

public class Main3<T extends Number> {
    T type;

    public Main3(T type) {
        this.type = type;
    }

    public static void main(String[] args) {
        Main3<Integer> main3 = new Main3<>(1);
        Main3<Double> main31 = new Main3<>(123.0);
        Main3 main32 = new Main3(1);

        main3.getSomething(main3);
//        main3.getSomething(main31);

        main3.getSomething1(main3);
        main3.getSomething1(main31);

        main3.getSomething2(main3);
        main3.getSomething2(main31);

    }

    public T getSomething(Main3<T> t) {
        System.out.println(t.getClass().getName());
        return t.type;
    }

    public void getSomething1(Main3<?> t) {
        System.out.println(t.getClass().getName());
    }

    public void getSomething2(Main3 t) {
        System.out.println(t.getClass().getName());
    }
}
