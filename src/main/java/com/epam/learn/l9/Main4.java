package com.epam.learn.l9;

public class Main4 {

    public static void main(String[] args) {
        try {
            new Main4().getFood("Barsik");
            new Main4().getFood("Murzik");
            new Main4().getFood("Snegok");
        } catch (IllegalArgumentException e) {
            System.err.println("Access denied");
        } catch (Exception e) {
            System.err.println("Something going wrong try later");
        }

        System.out.println();
    }

    void getFood(String name) {
        if (name.equals("Barsik")) {
            System.out.println("i am eating...");
        } else if(name.equals("Murzik")) {
            System.out.println("Murzik waiting 8 p.m.");
        } else {
            throw new IllegalArgumentException("Access to food restricted to: " + name);
        }

        System.out.println("food granted to: " +  name);
    }
}
