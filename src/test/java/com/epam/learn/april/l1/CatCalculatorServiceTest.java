package com.epam.learn.april.l1;

import org.junit.*;

import java.util.Date;

import static org.junit.Assert.assertEquals;

public class CatCalculatorServiceTest {
    int i = 0;

    @BeforeClass
    public static void globalInit() {

    }

    @AfterClass
    public static void globalCleanUp() {

    }

    @Before
    public void init() {
        i += i;
    }

    @After
    public void cleanUp() {
        System.out.println("после");
    }

    @Test(expected = RuntimeException.class)
    public void testGetCatCount() {
        CatCalculatorService service = new CatCalculatorService();

        int[] catArray1 = new int[]{};
        int[] catArray2 = new int[]{1};
        int[] catArray3 = new int[]{1, 2};
        int[] catArray4 = new int[]{1, 2, 3};

        assertEquals(0, service.getCatCount(catArray1));
        assertEquals(0, service.getCatCount(catArray2));
        assertEquals(2, service.getCatCount(catArray3));
        assertEquals(2, service.getCatCount(catArray4));
        throw new RuntimeException();
    }

    @Test(timeout = 1L)
    public void testGetCatCount1() throws InterruptedException {
        Date current = new Date();

        CatCalculatorService service = new CatCalculatorService();
//        TimeUnit.MILLISECONDS.sleep(1L);

        int[] catArray1 = new int[]{};
        int[] catArray2 = new int[]{1};
        int[] catArray3 = new int[]{1, 2};
        int[] catArray4 = new int[]{1, 2, 3};

        assertEquals(0, service.getCatCount(catArray1));
        assertEquals(0, service.getCatCount(catArray2));
        assertEquals(2, service.getCatCount(catArray3));
        assertEquals(2, service.getCatCount(catArray4));
        Date date = new Date();
        System.out.println(date.getTime() - current.getTime());
    }

    @Test
    public void testGetCatCount2() {
        CatCalculatorService service = new CatCalculatorService();

        int[] catArray1 = new int[]{};
        int[] catArray2 = new int[]{1};
        int[] catArray3 = new int[]{1, 2};
        int[] catArray4 = new int[]{1, 2, 3};

        assertEquals(0, service.getCatCount(catArray1));
        assertEquals(0, service.getCatCount(catArray2));
        assertEquals(2, service.getCatCount(catArray3));
        assertEquals(2, service.getCatCount(catArray4));
    }

    @Test
    public void testGetCatCount3() {
        CatCalculatorService service = new CatCalculatorService();

        int[] catArray1 = new int[]{};
        int[] catArray2 = new int[]{1};
        int[] catArray3 = new int[]{1, 2};
        int[] catArray4 = new int[]{1, 2, 3};

        assertEquals(0, service.getCatCount(catArray1));
        assertEquals(0, service.getCatCount(catArray2));
        assertEquals(2, service.getCatCount(catArray3));
        assertEquals(2, service.getCatCount(catArray4));
    }

    @Test
    public void testGetCatCount4() {
        CatCalculatorService service = new CatCalculatorService();

        int[] catArray1 = new int[]{};
        int[] catArray2 = new int[]{1};
        int[] catArray3 = new int[]{1, 2};
        int[] catArray4 = new int[]{1, 2, 3};

        assertEquals(0, service.getCatCount(catArray1));
        assertEquals(0, service.getCatCount(catArray2));
        assertEquals(2, service.getCatCount(catArray3));
        assertEquals(2, service.getCatCount(catArray4));
    }

    @Test
    public void testGetCatCount5() {
        CatCalculatorService service = new CatCalculatorService();

        int[] catArray1 = new int[]{};
        int[] catArray2 = new int[]{1};
        int[] catArray3 = new int[]{1, 2};
        int[] catArray4 = new int[]{1, 2, 3};

        assertEquals(0, service.getCatCount(catArray1));
        assertEquals(0, service.getCatCount(catArray2));
        assertEquals(2, service.getCatCount(catArray3));
        assertEquals(2, service.getCatCount(catArray4));
    }

    @Test
    public void testGetCatCount6() {
        CatCalculatorService service = new CatCalculatorService();

        int[] catArray1 = new int[]{};
        int[] catArray2 = new int[]{1};
        int[] catArray3 = new int[]{1, 2};
        int[] catArray4 = new int[]{1, 2, 3};

        assertEquals(0, service.getCatCount(catArray1));
        assertEquals(0, service.getCatCount(catArray2));
        assertEquals(2, service.getCatCount(catArray3));
        assertEquals(2, service.getCatCount(catArray4));
    }

    @Test
    public void testGetCatCount7() {
        CatCalculatorService service = new CatCalculatorService();

        int[] catArray1 = new int[]{};
        int[] catArray2 = new int[]{1};
        int[] catArray3 = new int[]{1, 2};
        int[] catArray4 = new int[]{1, 2, 3};

        assertEquals(0, service.getCatCount(catArray1));
        assertEquals(0, service.getCatCount(catArray2));
        assertEquals(2, service.getCatCount(catArray3));
        assertEquals(2, service.getCatCount(catArray4));
    }

    @Test
    public void testGetCatCount8() {
        CatCalculatorService service = new CatCalculatorService();

        int[] catArray1 = new int[]{};
        int[] catArray2 = new int[]{1};
        int[] catArray3 = new int[]{1, 2};
        int[] catArray4 = new int[]{1, 2, 3};

        assertEquals(0, service.getCatCount(catArray1));
        assertEquals(0, service.getCatCount(catArray2));
        assertEquals(2, service.getCatCount(catArray3));
        assertEquals(2, service.getCatCount(catArray4));
    }

    @Test
    public void testGetCatCount9() {
        CatCalculatorService service = new CatCalculatorService();

        int[] catArray1 = new int[]{};
        int[] catArray2 = new int[]{1};
        int[] catArray3 = new int[]{1, 2};
        int[] catArray4 = new int[]{1, 2, 3};

        assertEquals(0, service.getCatCount(catArray1));
        assertEquals(0, service.getCatCount(catArray2));
        assertEquals(2, service.getCatCount(catArray3));
        assertEquals(2, service.getCatCount(catArray4));
    }

    @Test
    public void testGetCatCount10() {
        CatCalculatorService service = new CatCalculatorService();

        int[] catArray1 = new int[]{};
        int[] catArray2 = new int[]{1};
        int[] catArray3 = new int[]{1, 2};
        int[] catArray4 = new int[]{1, 2, 3};

        assertEquals(0, service.getCatCount(catArray1));
        assertEquals(0, service.getCatCount(catArray2));
        assertEquals(2, service.getCatCount(catArray3));
        assertEquals(2, service.getCatCount(catArray4));
    }
}
