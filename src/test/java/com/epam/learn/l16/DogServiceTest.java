package com.epam.learn.l16;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class DogServiceTest {

    @Test(expected = RuntimeException.class)
    public void expectRuntimeWithoutPermissions() {
        // logic
        throw new RuntimeException();
    }

    @Test
    public void testGetDogCount() {
        int expectedDogCountWithTrue = 2;
        int expectedDogCountWithFalse = 1;
        DogService dogService = new DogService();
        assertEquals(expectedDogCountWithTrue, dogService.getDogCount(true));
        assertEquals(expectedDogCountWithFalse, dogService.getDogCount(false));
    }
}
